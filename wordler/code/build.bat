@echo off

SET CompilerFlags=/nologo /Z7 /Od /Oi /fp:fast /FC /W4 /WX -wd4189 -wd4100 -wd4505 -wd4456 -D_CRT_SECURE_NO_WARNINGS
SET LinkerFlags=/incremental:no

IF NOT EXIST build mkdir build
pushd build

cl.exe %CompilerFlags% ..\wordler\code\wordler_data_gen.cpp /link %LinkerFlags%
REM pushd "../wordler/data"
REM call W:/build/wordler_data_gen.exe words.txt google_10k_frequency.txt 4 WordDataFour ../code/word_data_four.h
REM call W:/build/wordler_data_gen.exe words.txt google_10k_frequency.txt 5 WordDataFive ../code/word_data_five.h
REM call W:/build/wordler_data_gen.exe words.txt google_10k_frequency.txt 6 WordDataSix ../code/word_data_six.h
REM call W:/build/wordler_data_gen.exe words.txt google_10k_frequency.txt 7 WordDataSeven ../code/word_data_seven.h
REM call W:/build/wordler_data_gen.exe five_letters_official.txt google_10k_frequency.txt 5 WordDataFive_Official ../code/word_data_five_official.h
REM popd

cl.exe %CompilerFlags% ..\wordler\code\word_list_length.cpp /link %LinkerFlags%
cl.exe %CompilerFlags% ..\wordler\code\win32_wordler.cpp /link %LinkerFlags%
cl.exe %CompilerFlags% ..\wordler\code\wasm_wordler.cpp /link %LinkerFlags%
cl.exe %CompilerFlags% ..\wordler\code\histograms.cpp /link %LinkerFlags%

popd
