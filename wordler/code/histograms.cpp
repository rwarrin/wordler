#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>

#include "wordler_common.h"
#include "word_data_five.h"

#define ArrayCount(Array) (sizeof(Array) / sizeof((Array)[0]))
#define MAX(A, B) ((A) > (B) ? A : B)
#define MIN(A, B) ((A) < (B) ? A : B)

struct sort_key
{
    size_t Index;
    float Weight;
};

static int32_t
SortKeyComparison(const void *Left, const void *Right)
{
    int32_t Result = 0;

    sort_key *A = (sort_key *)Left;
    sort_key *B = (sort_key *)Right;

    if(A->Weight > B->Weight)
    {
        Result = -1;
    }
    else if(A->Weight < B->Weight)
    {
        Result = 1;
    }

    return(Result);
}

int main(void)
{
    uint32_t CharacterOccuranceTable[128] = {0};
    uint32_t CharacterPositionTable[128][8] = {0};

    for(uint32_t WordIndex = 0; WordIndex < ArrayCount(WordDataFive); ++WordIndex)
    {
        uint8_t CharacterIndex = 0;
        for(char *Character = (char *)WordDataFive[WordIndex].Word; *Character; ++Character)
        {
            ++CharacterOccuranceTable[*Character];
            ++CharacterPositionTable[*Character][CharacterIndex++];
        }
    }

    uint32_t BestCharacterByIndex[8] = {0};
    uint32_t MaxFrequencyByIndex[8] = {0};

    for(uint32_t CharacterIndex = 'a'; CharacterIndex <= 'z'; ++CharacterIndex)
    {
        printf("%c: %5u    ", CharacterIndex, CharacterOccuranceTable[CharacterIndex]);
        for(uint32_t PositionIndex = 0;
            PositionIndex < ArrayCount(CharacterPositionTable[CharacterIndex]);
            ++PositionIndex)
        {
            uint32_t CharacterCountForCurrentPosition = CharacterPositionTable[CharacterIndex][PositionIndex];
            printf("[%5u]", CharacterCountForCurrentPosition);

            uint32_t BestCountForCharacterAtPosition = BestCharacterByIndex[PositionIndex] & 0xFFFF;
            if(CharacterCountForCurrentPosition > BestCountForCharacterAtPosition)
            {
                BestCharacterByIndex[PositionIndex] = ((CharacterIndex << 16) | (CharacterCountForCurrentPosition));
            }

            MaxFrequencyByIndex[PositionIndex] = MAX(CharacterCountForCurrentPosition, MaxFrequencyByIndex[PositionIndex]);
        }
        printf("\n");
    }

    for(uint32_t PositionIndex = 0; PositionIndex < ArrayCount(BestCharacterByIndex); ++PositionIndex)
    {
        uint32_t BestCharacterAtPosition = ((BestCharacterByIndex[PositionIndex] >> 16) & 0xFFFF);
        uint32_t BestCharacterCount = (BestCharacterByIndex[PositionIndex] & 0xFFFF);
        printf("[%c - %5u]", BestCharacterAtPosition, BestCharacterCount);
    }
    printf("\n");
    for(uint32_t PositionIndex = 0; PositionIndex < ArrayCount(BestCharacterByIndex); ++PositionIndex)
    {
        printf("[%5u]", MaxFrequencyByIndex[PositionIndex]);
    }
    printf("\n");

    float PositionWeightByWordLength = 1.0f / 5.0f;
    float CharacterWeightByIndex[128][8] = {0};
    for(uint32_t CharacterIndex = 'a'; CharacterIndex <= 'z'; ++CharacterIndex)
    {
        printf("%c: ", CharacterIndex);
        for(uint32_t PositionIndex = 0; PositionIndex < ArrayCount(BestCharacterByIndex); ++PositionIndex)
        {
            if(MaxFrequencyByIndex[PositionIndex])
            {
                float CharacterWeightForIndex = PositionWeightByWordLength *
                    ((float)CharacterPositionTable[CharacterIndex][PositionIndex] /
                     (float)MaxFrequencyByIndex[PositionIndex]);
                CharacterWeightByIndex[CharacterIndex][PositionIndex] = CharacterWeightForIndex;
            }
                
            printf("[%01.5f]", CharacterWeightByIndex[CharacterIndex][PositionIndex]);

        }
        printf("\n");
    }

    sort_key *SortList = (sort_key *)malloc(sizeof(*SortList)*ArrayCount(WordDataFive));
    for(uint32_t WordIndex = 0; WordIndex < ArrayCount(WordDataFive); ++WordIndex)
    {
        float Weight = 0.0f;
        uint8_t CharacterIndex = 0;
        for(char *Character = (char *)WordDataFive[WordIndex].Word; *Character; ++Character)
        {
            Weight += CharacterWeightByIndex[*Character][CharacterIndex++];
        }

        sort_key *SortKey = SortList + WordIndex;
        SortKey->Index = WordIndex;
        SortKey->Weight = Weight;
    }

    qsort(SortList, ArrayCount(WordDataFive), sizeof(*SortList), SortKeyComparison);

    for(uint32_t SortIndex = 0; SortIndex < MIN(INT_MAX, ArrayCount(WordDataFive)); ++SortIndex)
    {
        sort_key *SortKey = SortList + SortIndex;
        printf("%01.5f - %s\n", SortKey->Weight, WordDataFive[SortKey->Index].Word);
    }

    free(SortList);

    return(0);
}
