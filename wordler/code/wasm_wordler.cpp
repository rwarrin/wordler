#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <assert.h>
#include <limits.h>

#include "wordler_common.h"
#include "wordler_wordtrie.h"
#include "wordler_sort.h"

#include "wordler_wordtrie.cpp"
#include "wordler_sort.cpp"
#include "wordler_search.cpp"

#include "wordler.h"

#include "word_data_four.h"
#include "word_data_five.h"
#include "word_data_six.h"
#include "word_data_seven.h"
#include "word_data_five_official.h"

static wordler_context *GlobalWordlerContext_ = 0;

#if WORDLER_WASM
#undef assert
#define assert(...)
#endif

inline void
ApplyConstraintsToWordList(letter_node *Node, size_t Depth,
                           char *SearchString, size_t SearchLength,
                           uint32_t ExclusionSet, uint32_t InclusionSet,
                           uint32_t *PositionExclusions, uint32_t PositionExclusionsCount)
{
    assert(Node);

    if(Depth > SearchLength)
    {
        return;
    }

    if((Depth == SearchLength) && Node && Node->WordData)
    {
        uint32_t WordSet = 0;
        for(char *At = (char *)Node->WordData->Word; *At; ++At)
        {
            int32_t Index = GetIndex(*At);
            SetBit(WordSet, Index);
        }

        if((WordSet & InclusionSet) == InclusionSet)
        {
            Node->WordData->IsValid = true;
        }
    }

    if((Depth < SearchLength) && SearchString[Depth] &&
       (Depth < PositionExclusionsCount) &&
       (SearchString[Depth] != '?'))
    {
        size_t LetterIndex = GetIndex(SearchString[Depth]);
        if(Node->Letter[LetterIndex] && 
           !IsBitSet(PositionExclusions[Depth], LetterIndex))/* &&
           !IsBitSet(ExclusionSet, LetterIndex) &&
           !IsBitSet(PositionExclusions[Depth], LetterIndex))*/
        {
            ApplyConstraintsToWordList(Node->Letter[LetterIndex], Depth + 1,
                                       SearchString, SearchLength,
                                       ExclusionSet, InclusionSet,
                                       PositionExclusions, PositionExclusionsCount);
        }
    }
    else
    {
        for(size_t LetterIndex = 0; LetterIndex < ArrayCount(Node->Letter); ++LetterIndex)
        {
            if(Node->Letter[LetterIndex] &&
               !IsBitSet(ExclusionSet, LetterIndex) &&
               !IsBitSet(PositionExclusions[Depth], LetterIndex))
            {
                ApplyConstraintsToWordList(Node->Letter[LetterIndex], Depth + 1,
                                           SearchString, SearchLength,
                                           ExclusionSet, InclusionSet,
                                           PositionExclusions, PositionExclusionsCount);
            }
        }
    }
}

static void
SortResultsBufferByCharacterFrequency(wordler_context *WordlerContext, word_data *WordDataList,
                                      uint32_t WordListLength, uint32_t WordLength)
{
    if(WordDataList)
    {
        uint32_t CharacterPositionTable[128][MAX_WORD_LENGTH] = {0};
        for(uint32_t DataIndex = 0; DataIndex < WordListLength; ++DataIndex)
        {
            word_data *WordData = WordDataList + DataIndex;

            if(WordData->IsValid)
            {
                uint32_t CharacterIndex = 0;
                for(char *At = (char *)WordData->Word; !IsEndOfLine(*At); ++At)
                {
                    ++CharacterPositionTable[(*At) | 0x20][CharacterIndex++];
                }
            }
        }

        uint32_t MaxFrequencyByIndex[MAX_WORD_LENGTH] = {0};
        for(uint32_t CharacterIndex = 'a'; CharacterIndex <= 'z'; ++CharacterIndex)
        {
            for(uint32_t PositionIndex = 0;
                PositionIndex < ArrayCount(CharacterPositionTable[CharacterIndex]);
                ++PositionIndex)
            {
                MaxFrequencyByIndex[PositionIndex] = MAX(CharacterPositionTable[CharacterIndex][PositionIndex],
                                                         MaxFrequencyByIndex[PositionIndex]);
            }
        }

        float PositionWeightByWordLength = 1.0f / (float)WordLength;
        float CharacterWeightByIndexAndWordLength[128][MAX_WORD_LENGTH] = {0};
        for(uint32_t CharacterIndex = 'a'; CharacterIndex <= 'z'; ++CharacterIndex)
        {
            for(uint32_t PositionIndex = 0; PositionIndex < ArrayCount(MaxFrequencyByIndex); ++PositionIndex)
            {
                if(MaxFrequencyByIndex[PositionIndex])
                {
                    float CharacterWeightForIndex = ((float)CharacterPositionTable[CharacterIndex][PositionIndex] /
                                                     (float)MaxFrequencyByIndex[PositionIndex]);
                    WordlerContext->CharacterWeightByIndex[CharacterIndex][PositionIndex] = CharacterWeightForIndex;
                    CharacterWeightByIndexAndWordLength[CharacterIndex][PositionIndex] = PositionWeightByWordLength * CharacterWeightForIndex;
                }
            }
        }

        for(uint32_t DataIndex = 0; DataIndex < WordListLength; ++DataIndex)
        {
            word_data *WordData = WordDataList + DataIndex;
            WordData->Weight = 0.0f;

            if(WordData->IsValid)
            {
                uint32_t CharacterIndex = 0;
                for(char *At = (char *)WordData->Word; !IsEndOfLine(*At); ++At)
                {
                    WordData->Weight += CharacterWeightByIndexAndWordLength[(*At)|0x20][CharacterIndex++];
                }
            }
        }
    }
}

static void
SortResults(wordler_context *WordlerContext, word_data *WordDataList, uint32_t Size,
            sort_key *SortList, result_sort_type Type, uint32_t WordLength)
{
    if(WordDataList && SortList && Size)
    {
        if(Type == SortType_LetterFrequency)
        {
            SortResultsBufferByCharacterFrequency(WordlerContext, WordDataList, Size, WordLength);
        }

        for(uint32_t DataIndex = 0; DataIndex < Size; ++DataIndex)
        {
            word_data *WordData = WordDataList + DataIndex;
            sort_key *SortKey = SortList + DataIndex;

            SortKey->WordData = WordData;
            SortKey->Order = WordData->Ordinal;
            SortKey->Weight = WordData->Weight;
        }

        int32_t (*SortFunc)(const void *, const void *) = 0;
        switch(Type)
        {
            case SortType_LetterFrequency:
            {
                SortFunc = SortKeyByWeight;
            } break;

            case SortType_WordFrequency:
            {
                SortFunc = SortKeyByOrder;
            } break;

            case SortType_Alpha:
            default:
            {
                SortFunc = SortKeyByAlpha;
            } break;
        }

        if(SortFunc)
        {
            qsort(SortList, Size, sizeof(*SortList), SortFunc);
        }
    }
}

static size_t
_LoadWordlist(letter_node *Root, word_data *WordDataArray, size_t WordCount)
{
    size_t Result = 0;
    for(word_data *WordData = WordDataArray; WordCount--; ++WordData, ++Result)
    {
        InsertWord(Root, WordData);
    }

    return(Result);
}

static void
LoadWordlist(wordler_context *WordlerContext, word_list_dictionary Dictionary)
{
    uint32_t WordLength = 0;
    uint32_t WordCount = 0;
    word_data *WordData = 0;
    switch(Dictionary)
    {
        case WordListDictionary_Four:
        {
            WordData = WordDataFour;
            WordCount = ArrayCount(WordDataFour);
            WordLength = 4;
        } break;

        case WordListDictionary_Five_Official:
        {
            WordData = WordDataFive_Official;
            WordCount = ArrayCount(WordDataFive_Official);
            WordLength = 5;
        } break;

        case WordListDictionary_Six:
        {
            WordData = WordDataSix;
            WordCount = ArrayCount(WordDataSix);
            WordLength = 6;
        } break;

        case WordListDictionary_Seven:
        {
            WordData = WordDataSeven;
            WordCount = ArrayCount(WordDataSeven);
            WordLength = 7;
        } break;

        case WordListDictionary_Five:
        default:
        {
            WordData = WordDataFive;
            WordCount = ArrayCount(WordDataFive);
            WordLength = 5;
        } break;
    }

    if(WordData)
    {
        if(WordlerContext->WordTrieRoot)
        {
            FreeTrie(WordlerContext->WordTrieRoot);
        }

        WordlerContext->WordTrieRoot = NewLetterNode();
        WordlerContext->CurrentDictionary = Dictionary;

        WordlerContext->WordLength = WordLength;
        size_t WordsInserted = _LoadWordlist(WordlerContext->WordTrieRoot, WordData, WordCount);
        assert(WordCount == WordsInserted);

        WordlerContext->WordDataList = WordData;
        WordlerContext->WordDataListCount = WordCount;

        if(WordlerContext->SortList)
        {
            free(WordlerContext->SortList);
        }

        WordlerContext->SortListCount = WordCount;
        WordlerContext->SortList = (sort_key *)malloc(sizeof(*WordlerContext->SortList)*WordlerContext->SortListCount);
    }
    else
    {
        WordlerContext->CurrentDictionary = WordListDictionary_None;

        WordlerContext->WordLength = 0;

        WordlerContext->WordDataList = 0;
        WordlerContext->WordDataListCount = 0;

        WordlerContext->SortListCount = 0;
    }
}

inline void
PrintWordsToBuffer(char *Buffer, size_t BufferSize, sort_key *SortList, uint32_t Length)
{
    if(Buffer)
    {
        char *WritePtr = Buffer;
        for(uint32_t DataIndex = 0; DataIndex < Length; ++DataIndex)
        {
            sort_key *SortKeyEntry = SortList + DataIndex;
            word_data *WordData = SortKeyEntry->WordData;
            if(WordData->IsValid)
            {
                int32_t BytesWritten = sprintf(WritePtr, "%s\n", WordData->Word);
                WritePtr += BytesWritten;
                assert((uint32_t)(WritePtr - Buffer) <= BufferSize);
            }
        }
    }
}

extern "C" void
SetSearchString(char *InputString)
{
    for(uint32_t Index = 0; Index < ArrayCount(GlobalWordlerContext_->SearchString); ++Index)
    {
        GlobalWordlerContext_->SearchString[Index] = InputString[Index];
        if(InputString[Index] == 0)
        {
            break;
        }
    }
}

extern "C" void
SetExclusionList(char *CharacterList)
{
    GlobalWordlerContext_->ExclusionBitSet = 0;
    while(*CharacterList)
    {
        int32_t Index = GetIndex(*CharacterList);
        SetBit(GlobalWordlerContext_->ExclusionBitSet, Index);
        ++CharacterList;
    }
}

extern "C" void
SetInclusionList(char *CharacterList)
{
    GlobalWordlerContext_->InclusionBitSet = 0;
    while(*CharacterList)
    {
        int32_t Index = GetIndex(*CharacterList);
        SetBit(GlobalWordlerContext_->InclusionBitSet, Index);
        ++CharacterList;
    }
}

extern "C" void
SetPositionExclusions(int32_t Position, char *CharacterList)
{
    if((Position >= 0) && (Position < ArrayCount(GlobalWordlerContext_->PositionExclusionBitSet)))
    {
        GlobalWordlerContext_->PositionExclusionBitSet[Position] = 0;
        while(*CharacterList)
        {
            int32_t Index = GetIndex(*CharacterList);
            SetBit(GlobalWordlerContext_->PositionExclusionBitSet[Position], Index);
            ++CharacterList;
        }
    }
}

extern "C" void
SetSortType(result_sort_type SortType)
{
    GlobalWordlerContext_->ResultSortType = SortType;
}

extern "C" void
SetWordList(word_list_dictionary Dictionary)
{
    LoadWordlist(GlobalWordlerContext_, Dictionary);
}

static void
FindWords()
{
    if(GlobalWordlerContext_->WordTrieRoot && GlobalWordlerContext_->ResultsBuffer)
    {
        for(uint32_t WordIndex = 0; WordIndex < GlobalWordlerContext_->WordDataListCount; ++WordIndex)
        {
            word_data *WordData = GlobalWordlerContext_->WordDataList + WordIndex;
            WordData->IsValid = false;
        }

        GlobalWordlerContext_->SearchString[GlobalWordlerContext_->WordLength] = 0;
        ApplyConstraintsToWordList(GlobalWordlerContext_->WordTrieRoot, 0,
                                   GlobalWordlerContext_->SearchString, strlen(GlobalWordlerContext_->SearchString),
                                   GlobalWordlerContext_->ExclusionBitSet, GlobalWordlerContext_->InclusionBitSet,
                                   GlobalWordlerContext_->PositionExclusionBitSet, ArrayCount(GlobalWordlerContext_->PositionExclusionBitSet));

        SortResults(GlobalWordlerContext_,
                    GlobalWordlerContext_->WordDataList, GlobalWordlerContext_->WordDataListCount,
                    GlobalWordlerContext_->SortList, GlobalWordlerContext_->ResultSortType,
                    GlobalWordlerContext_->WordLength);

        PrintWordsToBuffer(GlobalWordlerContext_->ResultsBuffer, GlobalWordlerContext_->ResultsBufferSize,
                           GlobalWordlerContext_->SortList, GlobalWordlerContext_->WordDataListCount);
    }
}

extern "C" void
GetWords()
{
    FindWords();
    printf("%s", GlobalWordlerContext_->ResultsBuffer);
}

extern "C" void
GetCharacterFrequencies(char *Buffer, uint32_t BufferSize, uint32_t *BytesWritten)
{
    wordler_context *WordlerContext = GlobalWordlerContext_;

    SetSortType(SortType_LetterFrequency);
    FindWords();


    char *WriteAt = Buffer;
    WriteAt += sprintf(WriteAt, "[");
    for(uint32_t CharacterIndex = 'a'; CharacterIndex <= 'z'; ++CharacterIndex)
    {
        WriteAt += sprintf(WriteAt, "{\"Letter\": \"%c\",\"Frequencies\": [", CharacterIndex);
        for(uint32_t PositionIndex = 0; PositionIndex < WordlerContext->WordLength; ++PositionIndex)
        {
            WriteAt += sprintf(WriteAt, "%s\"%0.03f\"",
                               ((PositionIndex == 0) ? "" : ", "),
                               WordlerContext->CharacterWeightByIndex[CharacterIndex][PositionIndex]);

        }
        WriteAt += sprintf(WriteAt, "]}%s",
                           ((CharacterIndex == 'z') ? "" : ","));
    }
    WriteAt += sprintf(WriteAt, "]");
    *BytesWritten = (uint32_t)(WriteAt - Buffer);

    printf("-----\n%s\n", Buffer);
}

extern "C" void
Reset()
{
    GlobalWordlerContext_->ResultSortType = SortType_Alpha;
    GlobalWordlerContext_->ExclusionBitSet = 0;
    GlobalWordlerContext_->InclusionBitSet = 0;

    ZeroSize(GlobalWordlerContext_->SearchString, ArrayCount(GlobalWordlerContext_->SearchString));
    ZeroSize(GlobalWordlerContext_->PositionExclusionBitSet, ArrayCount(GlobalWordlerContext_->PositionExclusionBitSet));
}

extern "C" int main(int32_t ArgCount, char **Args)
{
    printf("Initializing... ");

    GlobalWordlerContext_ = (wordler_context *)malloc(sizeof(*GlobalWordlerContext_));
    ZeroSize(GlobalWordlerContext_, sizeof(*GlobalWordlerContext_));

    LoadWordlist(GlobalWordlerContext_, WordListDictionary_Five);

    size_t MaxWordListLength = MAX(MAX(MAX(ArrayCount(WordDataFour), ArrayCount(WordDataFive)),
                                   MAX(ArrayCount(WordDataSix), ArrayCount(WordDataSeven))),
                                   ArrayCount(WordDataFive_Official));
    GlobalWordlerContext_->ResultsBufferSize = (uint32_t)(sizeof(char) * (MaxWordListLength*2) * MAX_WORD_LENGTH);
    GlobalWordlerContext_->ResultsBuffer = (char *)malloc(GlobalWordlerContext_->ResultsBufferSize);
    assert(GlobalWordlerContext_->ResultsBuffer);

    GlobalWordlerContext_->ResultSortType = SortType_Alpha;

#if 0
    char TBuffer[8*1024] = {0};
    SetWordList(WordListDictionary_Five);
    printf("\n\n");
    SetSearchString("?????");
    GetCharacterFrequencies(TBuffer, ArrayCount(TBuffer));
    printf(TBuffer);
    GetWords();
    SetInclusionList("e");
    SetExclusionList("r");
    SetPositionExclusions(0, "a");
    GetWords();
    printf("\n\n");
    SetSortType(SortType_WordFrequency);
    GetWords();
    printf("\n\n");
    SetSortType(SortType_LetterFrequency);
    GetWords();
    Reset();
    printf("\n\n");
#endif

    printf("Done\n");

    return 0;
}
