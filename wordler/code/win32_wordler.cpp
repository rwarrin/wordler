#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <assert.h>

#define ArrayCount(Array) (sizeof(Array) / sizeof((Array)[0]))
#define ToggleBit(Bits, Index) (Bits ^= (1 << (Index)))
#define SetBit(Bits, Index) (Bits |= (1 << (Index)))
#define UnsetBit(Bits, Index) (Bits &= ~(1 << (Index)))
#define IsBitSet(Bits, Index) (Bits & (1 << (Index)))

struct letter_node
{
    letter_node *Letter[26];
    letter_node *Next;
    int8_t IsWord;
};

static letter_node *GlobalLetterNodeFreeList = 0;
static letter_node *
NewLetterNode()
{
    if(!GlobalLetterNodeFreeList)
    {
        size_t Count = 64;
        GlobalLetterNodeFreeList = (letter_node *)calloc(Count, sizeof(*GlobalLetterNodeFreeList));
        assert(GlobalLetterNodeFreeList);
        for(size_t Index = 0; Index < (Count - 1); ++Index)
        {
            GlobalLetterNodeFreeList[Index].Next = &GlobalLetterNodeFreeList[Index + 1];
        }
        GlobalLetterNodeFreeList[Count - 1].Next = 0;
    }

    letter_node *Result = GlobalLetterNodeFreeList;
    GlobalLetterNodeFreeList = Result->Next;
    Result->Next = 0;

    return(Result);
}

extern "C" void
FreeTrie(letter_node *Node)
{
    if(Node)
    {
        for(size_t Index = 0; Index < ArrayCount(Node->Letter); ++Index)
        {
            FreeTrie(Node->Letter[Index]);

            Node->IsWord = 0;
            Node->Next = GlobalLetterNodeFreeList;
            GlobalLetterNodeFreeList = Node;
        }
    }
}

inline int32_t
GetIndex(char C)
{
    C = C | 0x20;
    int32_t Result = (C - 'a');
    return(Result);
}

static void
InsertWord(letter_node *Node, char *Word)
{
    if((*Word == 0) || (*Word == '\n'))
    {
        Node->IsWord = 1;
        return;
    }

    int32_t Index = GetIndex(*Word);
    if(Node->Letter[Index] == 0)
    {
        Node->Letter[Index] = NewLetterNode();
    }

    InsertWord(Node->Letter[Index], Word + 1);
}

size_t Words = 0; // TODO(rick): Temp
inline void
PrintWords(letter_node *Node, char *Buffer, size_t BufferSize, size_t Depth,
           char *SearchString, size_t SearchLength,
           uint32_t ExclusionSet, uint32_t InclusionSet,
           uint32_t *PositionExclusions, uint32_t PositionExclusionsCount)
{
    assert(Node);

    if(Depth > SearchLength)
    {
        return;
    }

    if((Depth == SearchLength) && Node && Node->IsWord)
    {
        Buffer[Depth] = 0;

        uint32_t WordSet = 0;
        for(char *At = Buffer; *At; ++At)
        {
            int32_t Index = GetIndex(*At);
            SetBit(WordSet, Index);
        }

        if((WordSet & InclusionSet) == InclusionSet)
        {
            printf("%s\n", Buffer);
            ++Words; // TODO(rick): Temp
        }
    }

    if((Depth < SearchLength) && SearchString[Depth] &&
       (Depth < PositionExclusionsCount) &&
       (SearchString[Depth] != '*'))
    {
        size_t LetterIndex = GetIndex(SearchString[Depth]);
        if(Node->Letter[LetterIndex] &&
           !IsBitSet(ExclusionSet, LetterIndex) &&
           !IsBitSet(PositionExclusions[Depth], LetterIndex))
        {
            Buffer[Depth] = (char)('a' + LetterIndex);
            PrintWords(Node->Letter[LetterIndex], Buffer, BufferSize, Depth + 1,
                       SearchString, SearchLength,
                       ExclusionSet, InclusionSet,
                       PositionExclusions, PositionExclusionsCount);
        }
    }
    else
    {
        for(size_t LetterIndex = 0; LetterIndex < ArrayCount(Node->Letter); ++LetterIndex)
        {
            if(Node->Letter[LetterIndex] &&
               !IsBitSet(ExclusionSet, LetterIndex) &&
               !IsBitSet(PositionExclusions[Depth], LetterIndex))
            {
                Buffer[Depth] = (char)('a' + LetterIndex);
                PrintWords(Node->Letter[LetterIndex], Buffer, BufferSize, Depth + 1,
                           SearchString, SearchLength,
                           ExclusionSet, InclusionSet,
                           PositionExclusions, PositionExclusionsCount);
            }
        }
    }
}

inline int32_t
IsEndOfLine(char C)
{
    int32_t Result = ((C == '\r') ||
                      (C == '\n') ||
                      (C == 0));
    return(Result);
}

int main(void)
{
    printf("Hello world\n");
    // TODO(rick): If words are a prefix of another word then it's possible that
    // we will discover invalid words. We can fix this using an intern table and
    // hashing words then we can double check found words against the word list
    // words to make sure it really is valid. For now we can get around it by
    // using a word list with word lengths exactly equal to the query length.
    FILE *File = fopen("five_letters.txt", "rb");
    if(File)
    {
        letter_node *Root = NewLetterNode();

        char ReadBuffer[16] = {0};
        while(fgets(ReadBuffer, ArrayCount(ReadBuffer), File) != 0)
        {
            InsertWord(Root, ReadBuffer); 
        }

        uint32_t PositionExclusions[5] = {0}; // TODO(rick): Array size should match word and search max length
        uint32_t ExclusionSet = 0;
        uint32_t InclusionSet = 0;
        char WordGenBuffer[32] = {0};
        char SearchString[32] = {0};
        printf("> ");
        while(fgets(SearchString, ArrayCount(SearchString), stdin) != 0)
        {
            if(SearchString[0] == '-')
            {
                if(SearchString[1] == '-')
                {
                    for(char *At = SearchString + 1; !IsEndOfLine(*At); ++At)
                    {
                        int32_t Index = GetIndex(*At);
                        UnsetBit(ExclusionSet, Index);
                    }
                }
                else
                {
                    for(char *At = SearchString + 1; !IsEndOfLine(*At); ++At)
                    {
                        int32_t Index = GetIndex(*At);
                        SetBit(ExclusionSet, Index);
                    }
                }
                printf("%d\n", ExclusionSet);
            }
            else if(SearchString[0] == '+')
            {
                if(SearchString[1] == '+')
                {
                    for(char *At = SearchString + 1; !IsEndOfLine(*At); ++At)
                    {
                        int32_t Index = GetIndex(*At);
                        UnsetBit(InclusionSet, Index);
                    }
                }
                else
                {
                    for(char *At = SearchString + 1; !IsEndOfLine(*At); ++At)
                    {
                        int32_t Index = GetIndex(*At);
                        SetBit(InclusionSet, Index);
                    }
                }
                printf("%d\n", InclusionSet);
            }
            else if(SearchString[0] == '@')
            {
                if(SearchString[1] == '@')
                {
                    char *At = SearchString + 2;
                    while(!IsEndOfLine(*At))
                    {
                        int32_t Index = (At[0] - '0') - 1;
                        if((Index >= 0) && (Index < ArrayCount(PositionExclusions)))
                        {
                            int32_t BitIndex = GetIndex(At[1]);
                            UnsetBit(PositionExclusions[Index], BitIndex);
                        }
                        At += 2;
                    }
                }
                else
                {
                    char *At = SearchString + 1;
                    while(!IsEndOfLine(*At))
                    {
                        int32_t Index = (At[0] - '0') - 1;
                        if((Index >= 0) && (Index < ArrayCount(PositionExclusions)))
                        {
                            int32_t BitIndex = GetIndex(At[1]);
                            SetBit(PositionExclusions[Index], BitIndex);
                        }
                        At += 2;
                    }
                }
            }
            else if(SearchString[0] == '!')
            {
                ExclusionSet = 0;
                InclusionSet = 0;
                printf("%d, %d, ", ExclusionSet, InclusionSet);
                for(uint32_t Position = 0; Position < ArrayCount(PositionExclusions); ++Position)
                {
                    PositionExclusions[Position] = 0;
                    printf("%d, ", PositionExclusions[Position]);
                }
                printf("\n");
            }
            else if(SearchString[0] == '?')
            {
                printf("Wordler Help\n");
                printf("\nCommands:\n");
                printf("\t? = Shows this help message\n");
                printf("\t! = Reset excluded character list.\n");
                printf("\t-[abc...z] = Exclude set of characters from results.\n");
                printf("\t--[abc...z] = Remove set of characters from exclusion set.\n");
                printf("\t+[abc...z] = Include set of characters in results.\n");
                printf("\t++[abc...z] = Remove set of characters from inclusion set.\n");
                printf("\t@[0-9][a-z]{1,} = Disallow character at position specified. Can specify multiple conditions.\n");
                printf("\t@@[0-9][a-z]{1,} = Allow character at position specified. Can specify multiple conditions.\n");
                printf("\n[a-z*]{5} = Five characters consisting of either letters or * used to guide the" \
                       "word generation algorithm.\n\n");
            }
            else
            {
                // NOTE(rick): Temporarily truncate input at 5 characters since
                // we're using a 5 character word list.
                SearchString[5] = 0;
                PrintWords(Root, WordGenBuffer, ArrayCount(WordGenBuffer), 0,
                           SearchString, strlen(SearchString),
                           ExclusionSet, InclusionSet,
                           PositionExclusions, ArrayCount(PositionExclusions));
                printf("%zd words\n", Words); // TODO(rick): Temp
                Words = 0;
            }

            printf("\n> ");
        }
    }
    return 0;
}
