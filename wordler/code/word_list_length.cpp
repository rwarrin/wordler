#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ArrayCount(Array) (sizeof(Array) / sizeof((Array)[0]))

int main(int ArgCount, char **Args)
{
    if(ArgCount != 4)
    {
        printf("Usage: %s [length] [output_file] [input_file]\n", Args[0]);
        printf("Generates a file named output_file with words from input_file" \
               "having exactly length characters.\n");
        return(-1);
    }
    else
    {
        int TargetLength = atoi(Args[1]);
        if(TargetLength > 0)
        {
            char *OutputName = Args[2];
            char *InputName = Args[3];
            FILE *FileIn = fopen(InputName, "rb");
            FILE *FileOut = fopen(OutputName, "wb");
            if(FileIn && FileOut)
            {
                char ReadBuffer[64] = {0};
                while(fgets(ReadBuffer, ArrayCount(ReadBuffer), FileIn) != 0)
                {
                    int Length = (int)strlen(ReadBuffer);
                    if((Length - 1) == TargetLength)
                    {
                        fwrite(ReadBuffer, 1, Length, FileOut);
                    }
                }

                fclose(FileIn);
                fclose(FileOut);
            }
        }
    }

    return(0);
}
