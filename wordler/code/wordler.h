#ifndef WORDLER_H

#define MAX_WORD_LENGTH 8

enum word_list_dictionary
{
    WordListDictionary_None = 0,

    WordListDictionary_Four,
    WordListDictionary_Five,
    WordListDictionary_Six,
    WordListDictionary_Seven,
    WordListDictionary_Five_Official,

    WordListDictionary_Count,
};

enum result_sort_type
{
    SortType_Alpha = 0,
    SortType_LetterFrequency,
    SortType_WordFrequency
};

struct wordler_context
{
    word_list_dictionary CurrentDictionary;
    result_sort_type ResultSortType;
    uint32_t WordLength;
    uint32_t ExclusionBitSet;
    uint32_t InclusionBitSet;
    uint32_t PositionExclusionBitSet[MAX_WORD_LENGTH];

    char *ResultsBuffer;
    uint32_t ResultsBufferSize;

    char SearchString[MAX_WORD_LENGTH];

    letter_node *WordTrieRoot;

    word_data *WordDataList;
    uint32_t WordDataListCount;

    sort_key *SortList;
    uint32_t SortListCount;

    float CharacterWeightByIndex[128][MAX_WORD_LENGTH];
};

#define WORDLER_H
#endif
