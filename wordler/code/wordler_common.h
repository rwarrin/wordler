#ifndef WORDLER_COMMON_H

#define ArrayCount(Array) (sizeof(Array) / sizeof((Array)[0]))
#define ToggleBit(Bits, Index) (Bits ^= (1 << (Index)))
#define SetBit(Bits, Index) (Bits |= (1 << (Index)))
#define UnsetBit(Bits, Index) (Bits &= ~(1 << (Index)))
#define IsBitSet(Bits, Index) (Bits & (1 << (Index)))

#define MAX(A, B) ((A) > (B) ? A : B)
#define MIN(A, B) ((A) < (B) ? A : B)

#define Assert(Condition) do { if(!(Condition)) { *(int *)0 = 0; } } while(0)

struct word_data
{
    const char *Word;
    float Weight;
    uint16_t Ordinal;
    uint16_t IsValid;
};

inline int32_t
GetIndex(char C)
{
    C = C | 0x20;
    int32_t Result = (C - 'a');
    return(Result);
}

inline int32_t
IsEndOfLine(char C)
{
    int32_t Result = ((C == '\r') ||
                      (C == '\n') ||
                      (C == 0));
    return(Result);
}

inline void
ZeroSize(void *Mem, size_t Size)
{
    uint8_t *Byte = (uint8_t *)Mem;
    while(Size--)
    {
        *Byte++ = 0;
    }
}

#define WORDLER_COMMON_H
#endif
