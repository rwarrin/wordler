#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <limits.h>
#include <assert.h>

#include "wordler_common.h"

struct hash_node
{
    word_data Data;
    hash_node *Next;
};

struct hash_table
{
    uint32_t Count;
    hash_node *Nodes[256];
};

struct file_result
{
    char *Contents;
    uint32_t Size;
};

inline uint32_t
GetHash(char *Word)
{
    // TODO(rick): BETTER HASH FUNCTION!
    // djb2 - http://www.cse.yorku.ca/~oz/hash.html
    uint32_t Hash = 5381;
    while(!IsEndOfLine(*Word))
    {
        Hash = ((Hash << 5) + Hash) + *Word++;
    }
    return(Hash);
}

static word_data *
HashTableInsert(hash_table *Table, char *Word)
{
    int32_t TableIndex = GetHash(Word) & (ArrayCount(Table->Nodes) - 1);
    hash_node *HashNode = 0;
    for(hash_node *Node = Table->Nodes[TableIndex]; Node; Node = Node->Next)
    {
        if(strcmp(Word, Node->Data.Word) == 0)
        {
            HashNode = Node;
            break;
        }
    }

    if(!HashNode)
    {
        HashNode = (hash_node *)malloc(sizeof(*HashNode));
        HashNode->Data.Word = Word;
        HashNode->Next = Table->Nodes[TableIndex];
        Table->Nodes[TableIndex] = HashNode;
        ++Table->Count;
    }

    word_data *Result = &HashNode->Data;
    return(Result);
}

static word_data *
HashTableGet(hash_table *Table, char *Word)
{
    word_data *Result = 0;

    int32_t TableIndex = GetHash(Word) & (ArrayCount(Table->Nodes) - 1);
    for(hash_node *Node = Table->Nodes[TableIndex]; Node; Node = Node->Next)
    {
        if(strcmp(Word, Node->Data.Word) == 0)
        {
            Result = &Node->Data;
            break;
        }
    }

    return(Result);
}

inline file_result
ReadEntireFile(char *FileName)
{
    file_result Result = {0};

    FILE *File = fopen(FileName, "rb");
    if(File)
    {
        fseek(File, 0, SEEK_END);
        Result.Size = (uint32_t)ftell(File);
        fseek(File, 0, SEEK_SET);

        Result.Contents = (char *)malloc(sizeof(*Result.Contents) * (Result.Size + 1));
        if(Result.Contents)
        {
            fread(Result.Contents, sizeof(char), Result.Size, File);
            Result.Contents[Result.Size] = 0;
        }
        else
        {
            Result.Contents = 0;
            Result.Size = 0;
        }

        fclose(File);
    }

    return(Result);
}

int main(int ArgCount, char **Args)
{
    if(ArgCount != 6)
    {
        printf("Usage: %s [word_list_file] [frequency_list_file] [word_length] [data_name] [output_file_name]\n", Args[0]);
        printf("\tword_list_file: File to read words in from and generate the data structure.\n");
        printf("\tfrequency_list_file: File to read words in order of frequency/popularity.\n");
        printf("\tword_length: The length that words must be to be included in the output.\n");
        printf("\tdata_name: Name to use for the variable to generate.\n");
        printf("\toutput_file_name: Name of the file to generate.\n");
        return(-1);
    }

    char *WordListFileName = Args[1];
    char *FrequencyListFileName = Args[2];
    uint32_t WordLength = atoi(Args[3]);
    char *DataName = Args[4];
    char *OutputFileName = Args[5];

    file_result WordListFile = ReadEntireFile(WordListFileName);
    file_result FrequencyListFile = ReadEntireFile(FrequencyListFileName);

    if(WordListFile.Size && FrequencyListFile.Size)
    {
        hash_table HashTable = {0};

        char *Begin = WordListFile.Contents;
        char *End = Begin;
        while(!IsEndOfLine(*Begin))
        {
            while(!IsEndOfLine(*End)) { ++End; }
            *End = 0;

            if((End - Begin) == WordLength)
            {
                word_data *WordData = HashTableInsert(&HashTable, Begin);
                if(WordData)
                {
                    assert(WordData->Word == Begin);
                    WordData->Weight = 0.0f;
                    WordData->Ordinal = UINT16_MAX;
                    WordData->IsValid = 0;
                }
            }

            Begin = ++End;
        }

        
        Begin = FrequencyListFile.Contents;
        End = Begin;
        uint32_t LineNumber = 0;
        while(!IsEndOfLine(*Begin))
        {
            while(!IsEndOfLine(*End)) { ++End; }
            *End = 0;

            if((End - Begin) == WordLength)
            {
                word_data *WordData = HashTableGet(&HashTable, Begin);
                if(WordData)
                {
                    assert(strcmp(WordData->Word, Begin) == 0);
                    WordData->Ordinal = (uint16_t)LineNumber;
                }
            }

            ++LineNumber;
            Begin = ++End;
        }

        FILE *OutputFile = fopen(OutputFileName, "wb");
        if(OutputFile)
        {
            fprintf(OutputFile, "word_data %s[] = {\n", DataName);
            for(uint32_t TableIndex = 0; TableIndex < ArrayCount(HashTable.Nodes); ++TableIndex)
            {
                for(hash_node *Node = HashTable.Nodes[TableIndex]; Node; Node = Node->Next)
                {
                    word_data *Data = &Node->Data;
                    fprintf(OutputFile, "\t{\"%s\", %01.05ff, %u, %u},\n",
                            Data->Word, Data->Weight, Data->Ordinal, Data->IsValid);
                }
            }
            fprintf(OutputFile, "};\n");
            fclose(OutputFile);
        }
    }

    return(0);
}
