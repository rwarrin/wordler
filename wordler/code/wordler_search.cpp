// IMPORTANT(rick) TODO(rick): We can do much better than this linear search,
// replace this with binary search. Need a tool to convert the list of words in
// frequency order to a list of words in alphabetical order with their order in
// frequency. `man bsearch`

static uint32_t 
GetWordFrequencyIndex(char *Word, size_t WordLength, char **WordList, size_t WordCount)
{
    uint32_t Result = UINT_MAX;
    for(uint32_t Index = 0; Index < WordCount; ++Index)
    {
        char *TestWord = WordList[Index];
        if(strncmp(Word, TestWord, WordLength) == 0)
        {
            Result = Index;
            break;
        }
    }

    return(Result);
}
