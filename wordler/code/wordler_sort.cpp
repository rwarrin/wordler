static int32_t
SortKeyByWeight(const void *Left, const void *Right)
{
    int32_t Result = 0;

    sort_key *A = (sort_key *)Left;
    sort_key *B = (sort_key *)Right;

    if(A->Weight > B->Weight)
    {
        Result = -1;
    }
    else if(A->Weight < B->Weight)
    {
        Result = 1;
    }

    return(Result);
}

static int32_t
SortKeyByOrder(const void *Left, const void *Right)
{
    int32_t Result = 0;

    sort_key *A = (sort_key *)Left;
    sort_key *B = (sort_key *)Right;

    if(A->WordData->Ordinal < B->WordData->Ordinal)
    {
        Result = -1;
    }
    else if(A->WordData->Ordinal > B->WordData->Ordinal)
    {
        Result = 1;
    }

    return(Result);
}

static int32_t
SortKeyByAlpha(const void *Left, const void *Right)
{
    sort_key *A = (sort_key *)Left;
    sort_key *B = (sort_key *)Right;

    int32_t Result = strcmp(A->WordData->Word, B->WordData->Word);
    return(Result);
}
