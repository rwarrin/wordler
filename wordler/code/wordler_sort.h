#ifndef WORDLER_SORT_H

struct sort_key
{
    char *Word;
    word_data *WordData;
    float Weight;
    uint32_t Order;
};

#define WORDLER_SORT_H
#endif
