static letter_node *_GlobalLetterNodeFreeList = 0;

static letter_node *
NewLetterNode()
{
    if(!_GlobalLetterNodeFreeList)
    {
        size_t Count = 64;
        _GlobalLetterNodeFreeList = (letter_node *)calloc(Count, sizeof(*_GlobalLetterNodeFreeList));
        assert(_GlobalLetterNodeFreeList);
        for(size_t Index = 0; Index < (Count - 1); ++Index)
        {
            _GlobalLetterNodeFreeList[Index].Next = &_GlobalLetterNodeFreeList[Index + 1];
        }
        _GlobalLetterNodeFreeList[Count - 1].Next = 0;
    }

    letter_node *Result = _GlobalLetterNodeFreeList;
    _GlobalLetterNodeFreeList = Result->Next;
    Result->Next = 0;

    return(Result);
}

static void
FreeTrie(letter_node *Node)
{
    if(Node)
    {
        size_t LetterCount = ArrayCount(Node->Letter);
        for(size_t Index = 0; Index < LetterCount; ++Index)
        {
            FreeTrie(Node->Letter[Index]);
            Node->Letter[Index] = 0;
        }

        Node->WordData = 0;
        Node->Next = _GlobalLetterNodeFreeList;
        _GlobalLetterNodeFreeList = Node;
    }
}

static void
InsertWord(letter_node *Node, word_data *WordData, uint32_t Depth = 0)
{
    if((WordData->Word[Depth] == 0) || (WordData->Word[Depth] == '\n'))
    {
        Node->WordData = WordData;
        return;
    }

    int32_t Index = GetIndex(WordData->Word[Depth]);
    if(Node->Letter[Index] == 0)
    {
        Node->Letter[Index] = NewLetterNode();
    }

    InsertWord(Node->Letter[Index], WordData, Depth + 1);
}

