#ifndef WORDLER_WORDTRIE_H

struct letter_node
{
    letter_node *Letter[26];
    letter_node *Next;
    word_data *WordData;
};

#define WORDLER_WORDTRIE_H
#endif
