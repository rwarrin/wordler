@echo off

SET loc=%cd:~0,-13%

SUBST W: /D
SUBST W: %loc%
pushd W:

call "C:\Program Files (x86)\Microsoft Visual Studio\2019\BuildTools\VC\Auxiliary\Build\vcvarsall.bat" x64
start D:\Development\Vim\vim80\gvim.exe

cls
